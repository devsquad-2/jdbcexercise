package jdbc.service;

public interface UserService {
	void userInputInsertRecord();
	void userChoice();
	void updateRecord();
	void deleteRecord();
	void getUserRecord();
	boolean userLogin();
	void userLogout();
}
