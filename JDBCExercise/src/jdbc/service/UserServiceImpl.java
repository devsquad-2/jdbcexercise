package jdbc.service;

import java.util.InputMismatchException;
import java.util.Scanner;
import jdbc.dao.UserDAO;
import jdbc.dao.UserDAOImpl;
import jdbc.pojo.User;

public class UserServiceImpl implements UserService {

	UserDAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	
	@Override
	public boolean userLogin() {
		scannerRef = new Scanner(System.in);
		int userID;
		
		while(true) {
			try {				
				System.out.println("Enter User ID: ");
				userID = scannerRef.nextInt();
				break;
				
			} catch (InputMismatchException e) {
				scannerRef.nextLine();
				System.out.println("Please enter a valid User ID.");
			}
		}
		
		System.out.println("Enter your password: ");
		String password = scannerRef.next();
		
		refUserDAO = new UserDAOImpl();
		return refUserDAO.userLogin(userID, password);
	}
	
	@Override
	public void userInputInsertRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID: ");
		int userLoginID = scannerRef.nextInt();
		
		System.out.println("Enter User Password: ");
		String userPassword = scannerRef.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.insertRecord(refUser);
		
		System.out.println("New user input recorded.");
		getUserRecord();
	}
	
	@Override
	public void deleteRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID to delete: ");
		int input = scannerRef.nextInt();
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.deleteRecord(input);
		
		System.out.println("User " + input + " has been deleted.");
	}
	
	@Override
	public void getUserRecord() {
		refUserDAO = new UserDAOImpl();
		refUserDAO.getUserRecord();
	}
	
	@Override
	public void updateRecord() {
		scannerRef = new Scanner(System.in);
		System.out.println("Enter User ID to update: ");
		int userID = scannerRef.nextInt();
		
		System.out.println("Enter your new password: ");
		String newPassword = scannerRef.next();
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.updateRecord(userID, newPassword);
		
		System.out.println("Your password has been updated.");
	}
	
	@Override
	public void userLogout() {
		refUserDAO = new UserDAOImpl();
		refUserDAO.userLogout();
	}

	@Override
	public void userChoice() {

		if (userLogin()) {
			System.out.println("Enter choice: ");
			System.out.println("1. Insert New User");
			System.out.println("2. Delete User");
			System.out.println("3. Get User Records");
			System.out.println("4. Update Record");
			System.out.println("5. Logout");
			
			scannerRef = new Scanner(System.in);
			int choice = scannerRef.nextInt();
			switch (choice) {
			case 1:
				userInputInsertRecord();
				break;
				
			case 2:
				deleteRecord();
				break;
				
			case 3:
				getUserRecord();
				break;
				
			case 4: 
				updateRecord();
				break;
				
			case 5:
				userLogout();
				break;

			default:
				System.out.println("Invalid option. Please choose from option 1 to 5.");
				break;
			}
		} else {
			System.out.println("Login failed. Please try again.");
		}
	}
}
