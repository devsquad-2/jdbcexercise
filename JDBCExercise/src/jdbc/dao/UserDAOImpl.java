package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.pojo.User;
import utility.DBUtility;

public class UserDAOImpl implements UserDAO{

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	
	@Override
	public void getUserRecord() {
		
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "select user_id,user_password from user";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			ResultSet results = refPreparedStatement.executeQuery();  

			System.out.println("Your User list is as follows: ");
			System.out.println(" ");
			System.out.printf("%-10s %-10s \n", "User ID", "User Password");
			
			while (results.next()) {
				int user_id = results.getInt("user_id");
				String user_password = results.getString("user_password");
				System.out.printf("%-10d %-10s \n", user_id, user_password);
			}
			
		} catch (SQLException e) {
			System.out.println("Exception handled while getting all user records.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertRecord(User refUser) {
				
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "insert into user(user_id,user_password) values(?,?)";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getUserID());
			refPreparedStatement.setString(2, refUser.getUserPassword());
			
			refPreparedStatement.execute();  
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	} 

	@Override
	public void deleteRecord(int input) {
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "delete from user where user_id=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1,input);

			refPreparedStatement.execute();  
	
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateRecord(int userID, String newPassword) {
		try {
			refConnection = DBUtility.getConnection();	
			
			String sqlQuery = "update user set user_password=? where user_id=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, newPassword);
			refPreparedStatement.setInt(2,userID);

			refPreparedStatement.execute();  

			
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public boolean userLogin(int user_id, String password) {
		
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "SELECT user_id FROM user WHERE user_id = ? AND user_password = ? LIMIT 1";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, user_id);
			refPreparedStatement.setString(2, password);

			ResultSet rs = refPreparedStatement.executeQuery();  
			
			if(rs.next()) {
				return true;
			} else {
				return false;
			}
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while getting all user record.");
			return false;
		}
	}

	
	@Override
	public void userLogout() {
		try {
			refConnection = DBUtility.getConnection();	
			refConnection.close();
			System.out.println("Closing Connection.. Have a nice day.");			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
